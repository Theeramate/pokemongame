
public class Pikachu extends Pokemon{
    public Pikachu(String name){
        super(name);
        hp = 300;
        mp = 30;
        dmg = 30;
    }
}