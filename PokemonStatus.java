
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

public class PokemonStatus extends JFrame{
    private ArrayList<Pokemon> pokemon;
    private Trainer trainer;
    
    public PokemonStatus(Trainer trainer){
        super("Pokemon Game");
        this.trainer = trainer;
        this.pokemon = trainer.getBag();

        Container c = getContentPane();
        for(Pokemon p:pokemon){
        JLabel name = new JLabel("Pokemon" + p.getName());
        JLabel hp = new JLabel("HP: "+p.getHP());
        JLabel mp = new JLabel("MP: "+p.getMP());
        JLabel dmg = new JLabel("DMG: "+p.getDMG());
        name.setBounds(800, 400, 100,60);
        hp.setBounds(800, 400, 100,60);
        mp.setBounds(800, 400, 100,60);
        dmg.setBounds(800, 400, 100,60);
        }
        
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }
}