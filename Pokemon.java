public abstract class Pokemon {
    protected String name;
    protected int maxhp;
    protected int maxmp;
    protected int hp;
    protected int exp;
    protected int mp;
    protected int dmg;
    


    public Pokemon(String name){
        this.name = name;
        this.hp=0;
        this.mp=0;
        this.exp=0;
        this.dmg=0;
        assert maxhp <= hp;
        assert maxmp <= mp;
    }

    /*public boolean damage(int value){
        int currentHP = hp-value;
        if(currentHP >=0 ){
             this.hp=currentHP;   
        }
        else{    
            this.hp = 0;   
        }
        return true;
    }*/

    //public abstract void attack(Pokemon enemy);

    public String getName(){
        return name;
    }

    public int getHP(){
        return hp;
    }

    public int getMP(){
        return mp;
    }

    public int getDMG(){
        return dmg;
    }
    public String toString(){
        return name;
    }


}