
public class Bulbasaur extends Pokemon {
    public Bulbasaur(String name){
        super(name);
        hp = 350;
        mp = 20;
        dmg = 30;
    }

}
