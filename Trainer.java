import java.util.*;
public class Trainer {
    private String name;
    private String sex;
    private ArrayList<Pokemon> bag ;
    private int money;
    private int potion;
    private int highpotion;
    private int Either;
    private int pokeball;
    private int greatball;
    private int ultraball;
    private int masterball;


    public Trainer(){
        money = 100;
        pokeball = 5;
        potion =0;
        highpotion = 0;
        Either = 0 ;
        greatball = 0;
        ultraball = 0;
        masterball = 0;
        bag = new ArrayList<Pokemon>();
    }
public void printPokemon(ArrayList<Pokemon> pokemon){
    System.out.print("Pokemon in Bag");
    for(Pokemon p:bag){
        System.out.print(p);
        }
    }

public String getName(){
    return name;
}
public ArrayList<Pokemon> getBag(){
    return bag;
} 

public void setSex(int e){
    if(e == 0)
        sex = "Male";
    else if(e == 1)
        sex = "Female";
}

public boolean setName(String name){
    this.name = name;
    return true;

}
public void addPokemon(Pokemon pokemon){
    bag.add(pokemon);
}

public String getSex(){
    return sex;
}
public int getPotion(){
    return potion;
}
public int getPokeball(){
    return pokeball;
}

public int getMoney() {
    return money;
}

public void addPotion(int p){
    switch (p){
        case 1 :potion+=1;
                money -= 50;
                break;
        case 2 :highpotion +=1;
                money -= 100;
                break;
        case 3 :Either +=1;
                money -= 50;
                break;
    }   
}

public void addPokeball(int p){
    switch (p){
        case 1 :pokeball+=1;
                 money -= 50;
                break;
        case 2 :greatball=1;
                money -= 100;
                break;
        case 3 :ultraball +=1;
                money -= 200;        
                 break;
        case 4 :masterball +=1;
                money -= 10000000;        
                break;
            }   
}

}