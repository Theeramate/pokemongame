
public class Charmander extends Pokemon {
    public Charmander(String name){
        super(name);
        hp = 350;
        mp = 25;
        dmg = 35;
    }

}